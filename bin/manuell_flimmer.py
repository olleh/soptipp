#!/usr/bin/env python3

import soptippenv
import time
from rpi_hardware_pwm import HardwarePWM

# ==== Setup hardware ====
pwm0 = HardwarePWM(pwm_channel=0, hz=2)
pwm1 = HardwarePWM(pwm_channel=1, hz=2)


# ==== Loop ====
last_min = 100

while True:
    pwm0.start(50) # full duty cycle
    pwm1.start(100) # full duty cycle
    print("Sätt lampa1: 50 - lampa2: 100")

    input('Press return to swap:')
    pwm0.stop()
    pwm1.stop()
    time.sleep(5)

    pwm0.start(100) # full duty cycle
    pwm1.start(50) # full duty cycle
    print("Sätt lampa1: 100 - lampa2: 50")
    input('Press return to swap:')
    pwm0.stop()
    pwm1.stop()
    time.sleep(5)


#pwm.change_duty_cycle(50)
#pwm.change_frequency(25_000)
input('Press return to stop:')   # use raw_input for Python 2

pwm0.stop()
pwm1.stop()
