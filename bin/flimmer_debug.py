#!/usr/bin/env python3

import soptippenv
import time
#from rpi_hardware_pwm import HardwarePWM

class HardwarePWM:
    pw = 99
    hz = 99

    def __init__(self, pwm_channel, hz):
        self.pw = pwm_channel
        self.hz = hz

    def start(self,dc):
        print("Channel " + str(self.pw) + " Start w/ dc:"+ str(dc))

    def stop(self):
        print("Channel " + str(self.pw) + "Stop!")




yta = ""
if soptippenv.HOSTNAME=='soptipp1':
    yta = 'lampa1'
else:
    yta = 'lampa2'

# ==== Setup hardware ====
pwm0 = HardwarePWM(pwm_channel=0, hz=80)
pwm1 = HardwarePWM(pwm_channel=1, hz=80)

pwm0.start(100) # full duty cycle
pwm1.start(100) # full duty cycle

# ==== Loop ====
last_min = 100
schema = soptippenv.read_schema()

while True:
    #if time.localtime().tm_min < last_min: # Minuterna har slagit över från 59 till 0
    if (time.localtime().tm_min%2 ==0): # < last_min: # Minuterna har slagit över från 59 till 0
        print("Ny Timme")
        hh = time.localtime().tm_hour
        mm = time.localtime().tm_min
        last_min = mm
        for x in schema:
            if x['tid'] == time.strftime("%H:00", time.localtime()):
                print(x)
                pwm0.stop()
                pwm1.stop()
                time.sleep(2)
                print("Sleep")
                pwm0.start(int(x[yta]))
                pwm1.start(int(x[yta]))
                # print(x['lampa1']) # skall vara sätt DC till... och logga
                # print(x['lampa2']) # skall vara sätt DC till... och logga        
                # print(int(x['lampa1'])+int(x['lampa2']))
                print("Sätt lampa1: " + x[yta] + " - lampa2: " + x[yta])
                soptippenv.logging.info("lampa1: " + x[yta] + " - lampa2: " + x[yta])
    else:
        last_min = time.localtime().tm_min
        print(time.strftime("%H:%M", time.localtime()) + " större minut " + str(last_min) ) 
    time.sleep(17)

  



#pwm.change_duty_cycle(50)
#pwm.change_frequency(25_000)
input('Press return to stop:')   # use raw_input for Python 2

pwm0.stop()
pwm1.stop()
