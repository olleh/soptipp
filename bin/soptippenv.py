#!/usr/bin/env python3

import urllib.request, urllib.error, urllib.parse
import logging
import socket
import os.path
import csv

# ==== Global consts ====

HOSTNAME = socket.gethostname().split(".")[0]

(BASE_PATH, tail) = os.path.split(os.path.dirname(os.path.abspath(__file__)))

COMMAND_DIR = 'bin/'
COMMAND_PATH = os.path.join(BASE_PATH, COMMAND_DIR)

CONFIG_DIR = 'conf/'
CONFIG_PATH = os.path.join(BASE_PATH, CONFIG_DIR)
# CONFIG_PATH = BASE_PATH + CONFIG_DIR

OUTPUT_DIR = 'to_send/'
OUTPUT_PATH = os.path.join(BASE_PATH, OUTPUT_DIR)
# OUTPUT_PATH = BASE_PATH + OUTPUT_DIR

ARCHIVE_DIR = 'archive/'
ARCHIVE_PATH = os.path.join(BASE_PATH, ARCHIVE_DIR)
# ARCHIVE_PATH = BASE_PATH + ARCHIVE_DIR

LOGFILE = OUTPUT_PATH + HOSTNAME + '.log'
SCHEMAFILE = os.path.join(CONFIG_PATH, 'lampschema.csv')

# ==== Logging ====

logging.basicConfig(format='%(levelname)s: %(asctime)s - %(message)s', filename=LOGFILE, level=logging.INFO)

# ==== Functions ====

def internet_on():
    for timeout in [1,5,10,15,30]:
        try:
            response=urllib.request.urlopen('http://google.com',timeout=timeout)
            return True
        except urllib.error.URLError as err: pass
    return False
  
def read_schema():
    with open(SCHEMAFILE, 'r') as file:
        reader = csv.DictReader(file)
        data = list(reader)
        return(data)
    

  
if __name__ == "__main__":
    print(read_schema())
