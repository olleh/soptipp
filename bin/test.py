#!/usr/bin/env python3


import RPi.GPIO as GPIO
from time import sleep

pwmpin1 = 12				# PWM pin1
pwmpin2 = 35				# PWM pin2

GPIO.setwarnings(False)			#disable warnings
GPIO.setmode(GPIO.BOARD)		#set pin numbering system

GPIO.setup(pwmpin1,GPIO.OUT)
GPIO.setup(pwmpin2,GPIO.OUT)

pi_pwm1 = GPIO.PWM(pwmpin1,4)		#create PWM instance with frequency
pi_pwm1.start(50)				#start PWM of required Duty Cycle
pi_pwm2 = GPIO.PWM(pwmpin2,4)		#create PWM instance with frequency
pi_pwm2.start(50)				#start PWM of required Duty Cycle

input('Press return to stop:')
