#!/usr/bin/env python3

import soptippenv
import time
import RPi.GPIO as GPIO


yta = ""
if soptippenv.HOSTNAME=='soptipp1':
    yta = 'lampa1'
else:
    yta = 'lampa2'

# ==== Setup hardware ====
pwmpin1 = 12				# PWM pin1
pwmpin2 = 35				# PWM pin2

freq = 60

GPIO.setwarnings(False)			#disable warnings
GPIO.setmode(GPIO.BOARD)		#set pin numbering system

GPIO.setup(pwmpin1,GPIO.OUT)
GPIO.setup(pwmpin2,GPIO.OUT)

pwm0 = GPIO.PWM(pwmpin1,freq)		#create PWM instance with frequency
pwm1 = GPIO.PWM(pwmpin2,freq)		#create PWM instance with frequency

pwm0.start(100) # full duty cycle
pwm1.start(100) # full duty cycle

# ==== Loop ====
last_min = 100
schema = soptippenv.read_schema()

while True:
    if time.localtime().tm_min < last_min: # Minuterna har slagit över från 59 till 0
        print("Ny Timme")
        last_min = time.localtime().tm_min # reset minuterna för att börja räkna upp igen
        for x in schema:
            if x['tid'] == time.strftime("%H:00", time.localtime()):
                print(x)
                pwm0.stop()
                pwm1.stop()
                time.sleep(5)
                pwm0.start(int(x[yta]))
                pwm1.start(int(x[yta]))
                # print(x['lampa1']) # skall vara sätt DC till... och logga
                # print(x['lampa2']) # skall vara sätt DC till... och logga        
                # print(int(x['lampa1'])+int(x['lampa2']))
                print("Sätt lampa1: " + x[yta] + " - lampa2: " + x[yta])
                soptippenv.logging.info("sw - lampa1: " + x[yta] + " - lampa2: " + x[yta] + " freq=" + str(freq))
    else:
        print(time.strftime("%H:%M", time.localtime()) + " större minut " + str(last_min) )
        last_min = time.localtime().tm_min # fortsätt räkna upp minuterna så att vi inte fastnar på 0
    time.sleep(25)

  



#pwm.change_duty_cycle(50)
#pwm.change_frequency(25_000)
input('Press return to stop:')   # use raw_input for Python 2

pwm0.stop()
pwm1.stop()
